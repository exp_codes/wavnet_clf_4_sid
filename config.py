SR = 16000
wav_dur = 1  # second
DATA_HOP = 0.2
label_encoder_path = 'tmp/label_encoder.joblib'
retrain_true = True
load_true = False
model_dir = './tmp/'
Train_Epochs = 1000
MONITOR = 'val_acc'  # 'val_loss'# 'val_acc'
PRE_PROCESS = False
use_vocal_without_music = True  # if False use just music without vocal
use_ljj = False
use_wym = True
SHUFFLE = False
NORMALIZE = False
EARLY_STOP_STEP = 10

dataset_dir ='../mir1k_singer_vocal'#'../artist20_mp3s_32k_song_split'# '../mir1k_singer_vocal'  # '../artist20_mp3s_32k_album_split'  '#'../artist20_mp3s_32k_song_split' # 'artist20_mp3s_32k_song_split_music'
