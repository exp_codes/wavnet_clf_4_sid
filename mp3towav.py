import os
from pydub import AudioSegment

from main import vad_filter_out_silence


def do_trans(data_dir):
    for root, dirs, names in os.walk(data_dir):
        for name in names:
            if '.mp3' in name:
                mp3_path = os.path.join(root, name)
                print(mp3_path)
                wav_path = mp3_path.replace('.mp3', '.wav')
                sound = AudioSegment.from_mp3(mp3_path)
                sound = sound.set_frame_rate(16000)
                sound = sound.set_channels(1)
                sound.export(wav_path, 'wav')
                os.remove(mp3_path)


def do_vad(data_dir):
    for root, dirs, names in os.walk(data_dir):
        for name in names:
            if '.wav' in name:
                wav_path = os.path.join(root, name)
                print(wav_path)
                vad_filter_out_silence(wav_path)


def do_increase_db(data_dir, db=40):
    for root, dirs, names in os.walk(data_dir):
        for name in names:
            if '.wav' in name:
                wav_path = os.path.join(root, name)
                print(wav_path)
                song = AudioSegment.from_wav(wav_path)
                song += db
                song.export(wav_path, 'wav')
    return 0


if __name__ == '__main__':
    do_str = '2'
    if '1' in do_str:
        do_trans('../artist20_mp3s_32k_album_split')
    if '2' in do_str:
        do_vad('../artist20_mp3s_32k_song_split_music')
    if '3' in do_str:
        do_increase_db('../artist20_mp3s_32k_song_split_music')
