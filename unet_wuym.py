#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  1 11:47:06 2017

@author: wuyiming
"""
from pydub import AudioSegment
from librosa.util import find_files
from librosa.core import stft, load, istft, resample
from librosa.output import write_wav
import os.path

from chainer import Chain, serializers, optimizers, cuda, config
import chainer.links as L
import chainer.functions as F
import numpy as np

from config import dataset_dir

cp = cuda.cupy
SR = 16000
H = 512
FFT_SIZE = 1024
BATCH_SIZE = 64
PATCH_LENGTH = 128

PATH_FFT = "/media/wuyiming/TOSHIBA EXT/UNetFFT/"


class UNet(Chain):
    def __init__(self):
        super(UNet, self).__init__()
        with self.init_scope():
            self.conv1 = L.Convolution2D(1, 16, 4, 2, 1)
            self.norm1 = L.BatchNormalization(16)
            self.conv2 = L.Convolution2D(16, 32, 4, 2, 1)
            self.norm2 = L.BatchNormalization(32)
            self.conv3 = L.Convolution2D(32, 64, 4, 2, 1)
            self.norm3 = L.BatchNormalization(64)
            self.conv4 = L.Convolution2D(64, 128, 4, 2, 1)
            self.norm4 = L.BatchNormalization(128)
            self.conv5 = L.Convolution2D(128, 256, 4, 2, 1)
            self.norm5 = L.BatchNormalization(256)
            self.conv6 = L.Convolution2D(256, 512, 4, 2, 1)
            self.norm6 = L.BatchNormalization(512)
            self.deconv1 = L.Deconvolution2D(512, 256, 4, 2, 1)
            self.denorm1 = L.BatchNormalization(256)
            self.deconv2 = L.Deconvolution2D(512, 128, 4, 2, 1)
            self.denorm2 = L.BatchNormalization(128)
            self.deconv3 = L.Deconvolution2D(256, 64, 4, 2, 1)
            self.denorm3 = L.BatchNormalization(64)
            self.deconv4 = L.Deconvolution2D(128, 32, 4, 2, 1)
            self.denorm4 = L.BatchNormalization(32)
            self.deconv5 = L.Deconvolution2D(64, 16, 4, 2, 1)
            self.denorm5 = L.BatchNormalization(16)
            self.deconv6 = L.Deconvolution2D(32, 1, 4, 2, 1)

    def __call__(self, X):
        h1 = F.leaky_relu(self.norm1(self.conv1(X)))
        h2 = F.leaky_relu(self.norm2(self.conv2(h1)))
        h3 = F.leaky_relu(self.norm3(self.conv3(h2)))
        h4 = F.leaky_relu(self.norm4(self.conv4(h3)))
        h5 = F.leaky_relu(self.norm5(self.conv5(h4)))
        h6 = F.leaky_relu(self.norm6(self.conv6(h5)))
        dh = F.relu(F.dropout(self.denorm1(self.deconv1(h6))))
        dh = F.relu(F.dropout(self.denorm2(self.deconv2(F.concat((dh, h5))))))
        dh = F.relu(F.dropout(self.denorm3(self.deconv3(F.concat((dh, h4))))))
        dh = F.relu(self.denorm4(self.deconv4(F.concat((dh, h3)))))
        dh = F.relu(self.denorm5(self.deconv5(F.concat((dh, h2)))))
        dh = F.sigmoid(self.deconv6(F.concat((dh, h1))))
        return dh

    def load(self, fname="unet_wuym.model"):
        serializers.load_npz(fname, self)

    def save(self, fname="unet_wuym.model"):
        serializers.save_npz(fname, self)


class UNetTrainmodel(Chain):
    def __init__(self, unet):
        super(UNetTrainmodel, self).__init__()
        with self.init_scope():
            self.unet = unet

    def __call__(self, X, Y):
        O = self.unet(X)
        self.loss = F.mean_absolute_error(X * O, Y)
        return self.loss


def TrainUNet(Xlist, Ylist, epoch=40, savefile="unet_wuym.model"):
    assert (len(Xlist) == len(Ylist))
    unet = UNet()
    model = UNetTrainmodel(unet)
    model.to_gpu(0)
    opt = optimizers.Adam()
    opt.setup(model)
    config.train = True
    config.enable_backprop = True
    itemcnt = len(Xlist)
    itemlength = [x.shape[1] for x in Xlist]
    subepoch = sum(itemlength) // PATCH_LENGTH // BATCH_SIZE * 4
    for ep in range(epoch):
        sum_loss = 0.0
        for subep in range(subepoch):
            X = np.zeros((BATCH_SIZE, 1, 512, PATCH_LENGTH),
                         dtype="float32")
            Y = np.zeros((BATCH_SIZE, 1, 512, PATCH_LENGTH),
                         dtype="float32")
            idx_item = np.random.randint(0, itemcnt, BATCH_SIZE)
            for i in range(BATCH_SIZE):
                randidx = np.random.randint(
                    itemlength[idx_item[i]] - PATCH_LENGTH - 1)
                X[i, 0, :, :] = \
                    Xlist[idx_item[i]][1:, randidx:randidx + PATCH_LENGTH]
                Y[i, 0, :, :] = \
                    Ylist[idx_item[i]][1:, randidx:randidx + PATCH_LENGTH]
            opt.update(model, cp.asarray(X), cp.asarray(Y))
            sum_loss += model.loss.data * BATCH_SIZE

        print("epoch: %d/%d  loss=%.3f" % (ep + 1, epoch, sum_loss))

    unet.save(savefile)


def SaveSpectrogram(y_mix, y_vocal, y_inst, fname, original_sr=44100):
    y_mix = resample(y_mix, original_sr, SR)
    y_vocal = resample(y_vocal, original_sr, SR)
    y_inst = resample(y_inst, original_sr, SR)

    S_mix = np.abs(
        stft(y_mix, n_fft=FFT_SIZE, hop_length=H)).astype(np.float32)
    S_vocal = np.abs(
        stft(y_vocal, n_fft=FFT_SIZE, hop_length=H)).astype(np.float32)
    S_inst = np.abs(
        stft(y_inst, n_fft=FFT_SIZE, hop_length=H)).astype(np.float32)

    norm = S_mix.max()
    S_mix /= norm
    S_vocal /= norm
    S_inst /= norm

    np.savez(os.path.join(PATH_FFT, fname + ".npz"),
             mix=S_mix, vocal=S_vocal, inst=S_inst)


def LoadDataset(target="vocal"):
    filelist_fft = find_files(PATH_FFT, ext="npz")[:200]
    Xlist = []
    Ylist = []
    for file_fft in filelist_fft:
        dat = np.load(file_fft)
        Xlist.append(dat["mix"])
        if target == "vocal":
            assert (dat["mix"].shape == dat["vocal"].shape)
            Ylist.append(dat["vocal"])
        else:
            assert (dat["mix"].shape == dat["inst"].shape)
            Ylist.append(dat["inst"])
    return Xlist, Ylist


def LoadAudio(fname):
    y, sr = load(fname, sr=SR)
    dur = len(y) / sr
    y = np.concatenate((y, np.zeros(sr * 60)))
    spec = stft(y, n_fft=FFT_SIZE, hop_length=H, win_length=FFT_SIZE)
    mag = np.abs(spec)
    mag /= np.max(mag)
    phase = np.exp(1.j * np.angle(spec))
    return mag, phase, dur


def SaveAudio(fname, mag, phase, dur):
    y = istft(mag * phase, hop_length=H, win_length=FFT_SIZE)
    write_wav(fname, y[:int(dur * SR)], SR, norm=True)


def GetAudio(mag, phase, dur):
    y = istft(mag * phase, hop_length=H, win_length=FFT_SIZE)
    return y[:int(dur * SR)]


def ComputeMask(input_mag, unet_model="unet_wuym.model", hard=True):
    unet = UNet()
    unet.load(unet_model)
    config.train = False
    config.enable_backprop = False
    mask = unet(input_mag[np.newaxis, np.newaxis, 1:, :]).data[0, 0, :, :]
    mask = np.vstack((np.zeros(mask.shape[1], dtype="float32"), mask))
    if hard:
        hard_mask = np.zeros(mask.shape, dtype="float32")
        hard_mask[mask > 0.5] = 1
        return hard_mask
    else:
        return mask


def convert_wav_mp3(wav_path):
    song = AudioSegment.from_file(wav_path, 'wav')
    song.export(wav_path.replace('.wav', '.mp3'), 'mp3')
    return wav_path.replace('.wav', '.mp3')


def use_model_get_VA(mag, start, end):
    mask = ComputeMask(mag[:, start:end], unet_model="unet_wuym.model", hard=False)
    V = mag[:, start:end] * mask
    A = mag[:, start:end] * (1 - mask)
    return V, A


def split_it(fname):
    mag, phase, dur = LoadAudio(fname)

    start = 0
    end = mag.shape[-1] // 1024 * 1024
    V = 'V'
    A = 'A'
    for item in range(start, end, 1024):
        if type(V) == str and type(A) == str:
            V, A = use_model_get_VA(mag, item, item + 1024)
        else:
            Vv, Aa = use_model_get_VA(mag, item, item + 1024)
            V = np.hstack((V, Vv))
            A = np.hstack((A, Aa))
    vocal = GetAudio(V, phase[:, start:end], dur)
    accom = GetAudio(A, phase[:, start:end], dur)
    # SaveAudio(
    #     "%s-V.wav" % fname.strip('.wav'), V, phase[:, start:end], dur)
    # SaveAudio(
    #     "%s-A.wav" % fname.strip('.wav'), A, phase[:, start:end], dur)
    # v_path = convert_wav_mp3("%s-V.wav" % fname.strip('.wav'))
    # a_path = convert_wav_mp3("%s-A.wav" % fname.strip('.wav'))
    return vocal, accom, SR


def ensure_dirs(path):
    the_dir = os.path.dirname(path)
    if not os.path.isdir(the_dir):
        os.makedirs(the_dir)
    return 0


if __name__ == '__main__':
    for root, dirs, names in os.walk(dataset_dir):
        for name in names:
            if '.wav' in name:
                wav_path = os.path.join(root, name)
                print(wav_path)
                vocal_path = wav_path.replace(dataset_dir, dataset_dir + '_vocal')
                music_path = wav_path.replace(dataset_dir, dataset_dir + '_music')
                ensure_dirs(vocal_path)
                ensure_dirs(music_path)
                if not os.path.isfile(vocal_path):
                    vocal, music, sample_rate = split_it(wav_path)
                    write_wav(vocal_path, vocal, sample_rate)
                    write_wav(music_path, music, sample_rate)
