import os
from mutagen.mp3 import MP3
import datetime



def count_dir_wav(dir_name):
    for which in ['train', 'test', 'valid']:
        count = 0
        total = 0
        for root, dirs, names in os.walk(os.path.join(dir_name, which)):
            for item in names:
                if '.mp3' in item:
                    mp3_file = os.path.join(root, item)
                    count += 1
                    audio = MP3(mp3_file)
                    duration = audio.info.length
                    total += duration
        print(which)
        print(count)
        print(total)
        print(str(datetime.timedelta(seconds=total)))


if __name__ == '__main__':
    count_dir_wav('../artist20_mp3s_32k_album_split')
