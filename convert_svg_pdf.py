import os
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sn
import joblib
from sklearn.metrics import confusion_matrix

from config import dataset_dir, label_encoder_path


def trans_svg_pdf(svg_dir='svg_pdf'):
    for item in os.listdir(svg_dir):
        if '.svg' in item:
            svg_path = os.path.join(svg_dir, item)
            cmd_trans = 'inkscape -D -z --file=%s --export-pdf=%s' % (svg_path, svg_path.replace('.svg', '.pdf'))
            print(cmd_trans)
            if not os.path.isfile(svg_path.replace('.svg', '.pdf')):
                os.system(cmd_trans)
    return 0


def sn_plot_confusion_matrix(y_true, y_pred, level):
    label_encoder=joblib.load(label_encoder_path)
    ytick_lable=label_encoder.inverse_transform(range(len(label_encoder.classes_)))
    xtick_label=[item[:2] for item in ytick_lable]
    plt.figure()
    sn.set()
    cm = confusion_matrix(y_true, y_pred)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    sn.heatmap(cm,xticklabels=xtick_label,yticklabels=ytick_lable)# TODO
    plt.xlabel('True')
    plt.ylabel('Predict')
    plt.savefig("svg_pdf/sn_cm_%s_%s.svg" % (level,dataset_dir.split('/')[-1]), format='svg')
    return 0
def replot_confusion_matrix():
    for level in ['segment','song']:
        test_y_class, predict_y_class, class_names=joblib.load('tmp/cm_%s_%s.joblib' % (level, dataset_dir.split('/')[-1]))
        sn_plot_confusion_matrix(test_y_class, predict_y_class, level)
    return 0


if __name__ == '__main__':
    replot_confusion_matrix()
    trans_svg_pdf('svg_pdf')