import os
import os

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy as np
from sklearn import (manifold, datasets)

import joblib
import librosa
import numpy as np
import tensorflow as tf
from keras import Model
from librosa import load as wavread
from librosa.display import specshow
from numpy import array
from matplotlib import pyplot as plt
from scipy import signal
from scipy.io.wavfile import write as wavwrite
from WaveNetClassifier import WaveNetClassifier
from config import *
from enhanced_unet_ljj import do_svs_with_enhanced_unet_ljj


def process_wav_wavdata(wav_path):
    if not PRE_PROCESS:
        # raw data
        wav_data, sample_rate = wavread(wav_path, SR, True)
    else:
        # remove backgroud music
        print('# remove backgroud music')
        wav_data, _, sample_rate = do_svs_with_enhanced_unet_ljj(wav_path)
        # get only vocal part

        # print('# get only vocal part')
        # try:
        #     wav_data, _ = svd_filter_vocal_segments(wav_data, sample_rate)
        # except:
        #     print('wav_data None')
        #     wav_data = 'None'

        if wav_data != 'None':
            wav_data = wav_data * 32767
            wav_data = wav_data.astype(np.int16)

    return wav_data, sample_rate


def show_data_plot_chn(data, chn, fs):
    fig, axs = plt.subplots(5, 8)
    fig.subplots_adjust(hspace=.5, wspace=.001)
    axs = axs.ravel()

    for item in range(chn):
        chn_data = data[start_point * fs:end_point * fs, item]

        axs[item].plot(chn_data)
        axs[item].axis('off')
        axs[item].set_title("%02d" % item)

        wavwrite('demo_wav/%02d.wav' % item, fs, chn_data)

    fig.savefig('img/embedding_wav.svg', format="svg", bbox_inches='tight')

    fig = plt.figure()
    fig.subplots_adjust(hspace=.5, wspace=.05)

    for item in range(chn):
        chn_data = data[start_point * fs:end_point * fs, item]

        D = librosa.amplitude_to_db(np.abs(librosa.stft(np.asfortranarray(chn_data))), ref=np.max)
        plt.subplot(5, 8, item + 1)
        specshow(D, y_axis='log')
        plt.axis('off')
        plt.title("%02d" % item)

    fig.savefig('img/embedding_spec.svg', format="svg", bbox_inches='tight')

    return 0


def demo_wav_view_mid_output(wav_path):
    wnc = WaveNetClassifier((int(wav_dur * SR),), (20,), kernel_size=2, dilation_depth=9, n_filters=40,
                            task='classification', load=load_true, load_dir=model_dir)

    wav_data, sample_rate = process_wav_wavdata(wav_path)

    data_x = []
    for item_data in range(0, len(wav_data), int(wav_dur * sample_rate)):
        seg_wav_data = wav_data[item_data:int(item_data + wav_dur * sample_rate)]
        if len(seg_wav_data) < wav_dur * sample_rate:
            continue
        data_x.append(seg_wav_data)

    res_ = wnc.predict(np.array(data_x))
    res_embedding_layer = wnc.embedding_layer.predict(np.array(data_x))
    res_embedding_layer = res_embedding_layer.reshape(-1, 40)

    res_downsample_to_200Hz_layer = wnc.downsample_to_200Hz_layer.predict(np.array(data_x))
    res_downsample_to_200Hz_layer = res_downsample_to_200Hz_layer.reshape(-1, 40)
    res_downsample_to_2Hz_layer = wnc.downsample_to_2Hz_layer.predict(np.array(data_x))
    res_downsample_to_2Hz_layer = res_downsample_to_2Hz_layer.reshape(-1, 20)
    res_bottle_neck_layer = wnc.bottle_neck_layer.predict(np.array(data_x))
    show_data_plot_chn(res_embedding_layer, 40, SR)
    wav_data = wav_data[start_point * SR:end_point * SR]
    plt.figure()
    plt.plot(wav_data)
    plt.show()
    wavwrite('demo_wav/raw.wav', SR, wav_data)
    print('debug')


def get_data_visualize_tsne_bottle_neck_layer():
    label_encoder = joblib.load(label_encoder_path)
    wnc = WaveNetClassifier((int(wav_dur * SR),), (20,), kernel_size=2, dilation_depth=9, n_filters=40,
                            task='classification', load=load_true, load_dir=model_dir)
    visual_dir = os.path.join(dataset_dir, 'train')
    all_train_data = []
    all_train_label = []
    for root, dirs, names in os.walk(visual_dir):
        for name in names:
            if '.wav' in name:
                wav_path = os.path.join(root, name)
                wav_data, sample_rate = process_wav_wavdata(wav_path)
                label = wav_path.split(os.sep)[3]
                data_x = []
                for item_data in range(0, len(wav_data), int(wav_dur * sample_rate)):
                    seg_wav_data = wav_data[item_data:int(item_data + wav_dur * sample_rate)]
                    if len(seg_wav_data) < wav_dur * sample_rate:
                        continue
                    data_x.append(seg_wav_data)

                res_bottle_neck_layer = wnc.bottle_neck_layer.predict(np.array(data_x))
                all_train_data.append(np.median(res_bottle_neck_layer, axis=0))

                all_train_label.append(label_encoder.transform([label])[0])



    return np.array(all_train_data), all_train_label


def visualization(X, y, figure_name="tsne"):
    label_encoder = joblib.load(label_encoder_path)
    # t-SNE embedding of the digits dataset
    print("Computing t-SNE embedding")
    tsne = manifold.TSNE(n_components=2, init='pca', random_state=0)
    X_tsne = tsne.fit_transform(X)

    # Scale and visualize the embedding vectors
    x_min, x_max = np.min(X_tsne, 0), np.max(X_tsne, 0)
    X_tsne = (X_tsne - x_min) / (x_max - x_min)
    colors = cm.rainbow(np.linspace(0, 1, len(set(y))))
    plt.figure()
    for i in range(X_tsne.shape[0]):
        plt.plot(X_tsne[i, 0], X_tsne[i, 1], 'o', color=colors[y[i]])
    plt.axis('off')

    for y_item in set(y):
        all_x_loc = []
        for x_loc, y_label in zip(X_tsne, y):
            if y_label == y_item:
                all_x_loc.append(x_loc)
        mean_x_loc = np.median(all_x_loc, 0)
        y_item_label = label_encoder.inverse_transform([y_item])[0]
        plt.annotate(y_item_label, mean_x_loc,
                     color='black')  # ,backgroundcolor=colors[y_item])

    plt.savefig(figure_name + '.svg', format="svg", bbox_inches='tight')
    # cmd_str = '"C:\Program Files\Inkscape\inkscape.exe" -D -z --file={0}.svg --export-pdf={0}.pdf'.format(figure_name)
    # print('inkscape svg to pdf...')
    # os.system(cmd_str)

    return 0


def view_stack_feature_maps():
    statck_data = []
    for wav in os.listdir('demo_wav'):
        if '.wav' in wav and 'raw' not in wav:
            wav_path = os.path.join('demo_wav', wav)
            wav_data,_ = wavread(wav_path,SR)
            statck_data.append(wav_data)
    total = np.sum(statck_data, 0)

    wavwrite('demo_wav/raw_stack.wav', 16000, total )

    raw_data,_= wavread('demo_wav/raw.wav')
    plt.figure()
    plt.subplot(211)
    plt.plot(total)

    plt.subplot(212)
    plt.plot(raw_data)

    plt.savefig('img/raw_statck_compare_wav.svg', format='svg')

    plt.figure()
    plt.subplot(211)
    total, _ = librosa.load('demo_wav/raw_stack.wav', 16000)
    D = librosa.amplitude_to_db(np.abs(librosa.stft(total)), ref=np.max)
    specshow(D, y_axis='log')

    plt.subplot(212)
    raw_data, _ = librosa.load('demo_wav/raw.wav', 16000)
    D = librosa.amplitude_to_db(np.abs(librosa.stft(raw_data)), ref=np.max)
    specshow(D, y_axis='log')

    plt.savefig('img/raw_statck_compare_spec.svg', format='svg')
    plt.show()

    return 0


if __name__ == '__main__':
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    print('view...')
    start_point = 22
    end_point = 27
    run_str = '2'

    if '1' in run_str:
        demo_wav_view_mid_output('test.wav')  # embedding layers
    if '2' in run_str:
        x, y = get_data_visualize_tsne_bottle_neck_layer()
        visualization(x, y, 'img/tsne-new')
    if '3' in run_str:
        view_stack_feature_maps()  # visualize the stack embedding layers
