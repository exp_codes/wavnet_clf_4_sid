import os

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy as np
from sklearn import (manifold, datasets)


def visualization(X, y, title="t-SNE embedding ", figure_name="img/tsne"):
    # t-SNE embedding of the digits dataset
    print("Computing t-SNE embedding")
    tsne = manifold.TSNE(n_components=2, init='pca', random_state=0)
    X_tsne = tsne.fit_transform(X)

    # Scale and visualize the embedding vectors
    x_min, x_max = np.min(X_tsne, 0), np.max(X_tsne, 0)
    X_tsne = (X_tsne - x_min) / (x_max - x_min)
    colors = cm.rainbow(np.linspace(0, 1, len(set(y))))
    plt.figure()
    for i in range(X_tsne.shape[0]):
        plt.plot(X_tsne[i, 0], X_tsne[i, 1], 'o', color=colors[y[i]])
    plt.axis('off')
    for y_item in set(y):
        all_x_loc = []
        for x_loc, y_label in zip(X_tsne, y):
            if y_label == y_item:
                all_x_loc.append(x_loc)
        mean_x_loc = np.mean(all_x_loc, 0)
        plt.annotate(y_item, mean_x_loc,
                     color='white',)
                     #backgroundcolor=colors[y_item])

    plt.savefig(figure_name + '.svg', format="svg", bbox_inches='tight')
    # cmd_str = '"C:\Program Files\Inkscape\inkscape.exe" -D -z --file={0}.svg --export-pdf={0}.pdf'.format(figure_name)
    # print('inkscape svg to pdf...')
    # os.system(cmd_str)

    return 0


if __name__ == '__main__':
    digits = datasets.load_digits(n_class=6)
    X = digits.data
    y = digits.target
    visualization(X, y)
