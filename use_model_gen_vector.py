import joblib
# from sklearn.grid_search import RandomizedSearchCV
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, cohen_kappa_score, confusion_matrix
from scipy.stats import randint as sp_randint
from sklearn.model_selection import RandomizedSearchCV

from WaveNetClassifier import WaveNetClassifier
from config import wav_dur, model_dir
from sklearn.ensemble import RandomForestClassifier
import os
from librosa import load as wavread
import numpy as np
from matplotlib import pyplot as plt
from main import roc_auc_score_multiclass


def evaluate_report(test_y_class, predict_y_class, level='segment'):
    print('%s\tlevel results:' % level)
    print("ground  truth:", test_y_class)
    print("predict lable:", predict_y_class)
    # accuracy: (tp + tn) / (p + n)
    accuracy = accuracy_score(test_y_class, predict_y_class)
    print('Accuracy: %f' % accuracy)
    # precision tp / (tp + fp)
    precision = precision_score(test_y_class, predict_y_class, average="macro")
    print('Precision: %f' % precision)
    # recall: tp / (tp + fn)
    recall = recall_score(test_y_class, predict_y_class, average="macro")
    print('Recall: %f' % recall)
    # f1: 2 tp / (2 tp + fp + fn)
    f1 = f1_score(test_y_class, predict_y_class, average="macro")
    print('F1 score: %f' % f1)

    # kappa
    kappa = cohen_kappa_score(test_y_class, predict_y_class)
    print('Cohens kappa: %f' % kappa)
    # ROC AUC
    auc = roc_auc_score_multiclass(test_y_class, predict_y_class)  # predict_y_prob)
    print('ROC AUC: ', auc)
    # confusion matrix
    matrix = confusion_matrix(test_y_class, predict_y_class)
    print(matrix)
    joblib.dump((test_y_class, predict_y_class), 'tmp/cm_%s_%s.joblib' % (level, dataset_name))
    plot_confusion_matrix(test_y_class, predict_y_class, set(test_y_class), normalize=True,
                          title='Normalized confusion matrix', level=level)

    return accuracy, recall, precision, f1, kappa, auc, matrix


def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues, level='segment'):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    # classes = classes[unique_labels(y_true)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)
    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    np.set_printoptions(precision=2)
    fig.savefig('tmp/confusion_matric_%s_%s.png' % (level, dataset_name), dpi=400, )
    return ax


def use_trained_model_get_data_x_label_y_segment_level(data_dir):
    global sample_rate
    dataX, lableY = [], []
    for root, dirs, names in os.walk(data_dir):
        for name in names:
            if '.wav' in name:
                wav_path = os.path.join(root, name)
                print(wav_path)
                wav_data, sample_rate = wavread(wav_path, sample_rate, True)
                singer_label = wav_path.split(os.sep)[3]

                for item_data in range(0, len(wav_data), wav_dur * sample_rate):
                    seg_wav_data = wav_data[item_data:(item_data + wav_dur * sample_rate)]
                    if len(seg_wav_data) < wav_dur * sample_rate:
                        continue

                    dataX.extend(wnc.predict(np.array([seg_wav_data])))
                    lableY.append(singer_label)
    return dataX, lableY


def use_trained_model_get_data_x_label_y_song_level(data_dir):
    global sample_rate
    dataX, lableY = [], []
    for root, dirs, names in os.walk(data_dir):
        for name in names:
            if '.wav' in name:
                wav_path = os.path.join(root, name)
                print(wav_path)
                wav_data, sample_rate = wavread(wav_path, sample_rate, True)
                singer_label = wav_path.split(os.sep)[3]
                song_level_dataX = []
                for item_data in range(0, len(wav_data), wav_dur * sample_rate):
                    seg_wav_data = wav_data[item_data:(item_data + wav_dur * sample_rate)]
                    if len(seg_wav_data) < wav_dur * sample_rate:
                        continue
                    song_level_dataX.extend(wnc.predict(np.array([seg_wav_data])))
                if len(song_level_dataX) != 0:
                    dataX.append(sum(song_level_dataX) / len(song_level_dataX))
                    lableY.append(singer_label)
    return dataX, lableY


def test_each_song_with_one_lable(wnc, clf, dataset_dir):
    global sample_rate
    ground_truth = []
    predict_singer = []
    for root, dirs, names in os.walk(dataset_dir):
        for name in names:
            if '.wav' in name:
                wav_path = os.path.join(root, name)
                wav_data, sample_rate = wavread(wav_path, sample_rate, True)
                print(sample_rate, wav_path)
                singer_label = wav_path.split(os.sep)[3]
                ground_truth.append(singer_label)
                data_x = []
                for item_data in range(0, len(wav_data), wav_dur * sample_rate):
                    seg_wav_data = wav_data[item_data:(item_data + wav_dur * sample_rate)]
                    if len(seg_wav_data) < wav_dur * sample_rate:
                        continue
                    data_x.append(seg_wav_data)
                wav_results_prob = clf.predict(wnc.predict(np.array(data_x)))
                wav_prob = np.sum(wav_results_prob, axis=0)
                wav_predict_num = wav_prob.argmax()

                predict_singer.append(wav_predict_num)

    accuracy, recall, precision, f1, kappa, auc, matrix = evaluate_report(ground_truth, predict_singer,
                                                                          level='song')

    return accuracy, recall, precision, f1, kappa, auc, matrix


if __name__ == '__main__':
    sample_rate = 16000
    wnc = WaveNetClassifier((wav_dur * sample_rate,), (6,), kernel_size=2, dilation_depth=9, n_filters=40,
                            task='classification', load=False, load_dir=model_dir)

    clf = RandomForestClassifier(n_estimators=1000, max_depth=5,
                                 random_state=0, )
    dataset_name = 'artist20_mp3s_32k_song_split'

    print('get data ...')
    trainX, trainY = use_trained_model_get_data_x_label_y_song_level('../%s/train' % dataset_name)

    validX, vilidY = use_trained_model_get_data_x_label_y_song_level('../%s/valid' % dataset_name)
    print('grid search ...')
    param_dist = {"max_depth": [3, None],
                  "max_features": sp_randint(1, 6),
                  "min_samples_split": sp_randint(2, 6),
                  "bootstrap": [True, False],
                  "criterion": ["gini", "entropy"]}
    n_iter_search = 20
    random_search = RandomizedSearchCV(clf, param_distributions=param_dist,
                                       n_iter=n_iter_search, cv=5, iid=False)

    random_search.fit(trainX, trainY)
    print("RandomizedSearchCV  for %d candidates"
          " parameter settings." % n_iter_search)
    clf = random_search.best_estimator_
    # clf.fit(trainX, trainY)
    print('predict ...')
    predictY = clf.predict(validX)
    print('report ...')
    accuracy, recall, precision, f1, kappa, auc, matrix = evaluate_report(vilidY, predictY,
                                                                          level='segment')
    collect_result_dict = {}
    collect_result_dict['song_level'] = {'accuracy': accuracy,
                                            'recall': recall,
                                            'precision': precision,
                                            'f1': f1,
                                            'kappa': kappa,
                                            'auc': auc,
                                            'matrix': matrix,
                                            'fig_acc': 'tmp/history_acc_%s_RF.png' % dataset_name,
                                            'fig_loss': 'tmp/history_loss_%s_RF.png' % dataset_name,
                                            'fig_cm': 'tmp/confusion_matric_segment_%s_RF.png' % dataset_name,
                                            'plot_cm_need': 'tmp/cm_segment_%s_RF.joblib' % dataset_name,
                                            }


    joblib.dump(collect_result_dict, 'tmp/collect_result_dict_%s_RF.joblib' % dataset_name)
