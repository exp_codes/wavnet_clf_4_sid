import csv
import os
import time
import sys
import joblib
import librosa
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sn
import tensorflow as tf
from keras.utils import to_categorical
from librosa import load as wavread
from librosa.feature import rms
from librosa.output import write_wav
from numpy import array
from sklearn import preprocessing
from sklearn.metrics import accuracy_score
from sklearn.metrics import cohen_kappa_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import roc_auc_score
from sklearn.utils import shuffle

from WaveNetClassifier import WaveNetClassifier
from config import *
from enhanced_unet_ljj import do_svs_with_enhanced_unet_ljj
from unet_wuym import split_it

config = tf.ConfigProto()
config.gpu_options.allow_growth = True


def vad_filter_out_silence(wav_path='test.wav'):
    data, sr = librosa.load(wav_path, 16000)
    rs = rms(data, hop_length=2048)[0]
    # plt.hist(rs)
    # plt.show()
    new_data = []
    for item in range(len(rs)):
        if rs[item] >= 0.00025:
            new_data.append(data[item * 2048:(item + 1) * 2048])

    new = np.concatenate(new_data)
    write_wav(wav_path, new, sr)
    # plt.subplot(211)
    # plt.plot(new)
    # plt.subplot(212)
    # plt.plot(data)
    # plt.show()

    return 0


def process_wav_wavdata(wav_path):
    if not PRE_PROCESS:
        # raw data
        wav_data, sample_rate = wavread(wav_path, SR, True)
    else:
        # remove backgroud music
        # print('# remove backgroud music')
        if use_ljj:
            vocal, music, sample_rate = do_svs_with_enhanced_unet_ljj(wav_path)
        elif use_wym:
            vocal, music, sample_rate = split_it(wav_path)
        else:
            vocal, music = None, None
        if use_vocal_without_music:
            wav_data = vocal
        else:
            wav_data = music
        # get only vocal part

        # print('# get only vocal part')
        # try:
        #     wav_data, _ = svd_filter_vocal_segments(wav_data, sample_rate)
        # except:
        #     print('wav_data None')
        #     wav_data = 'None'

        if wav_data != 'None':
            wav_data = wav_data * 32767
            wav_data = wav_data.astype(np.int16)

    return wav_data, sample_rate


def get_wav_dataset(dataset_dir):
    all_singers = []
    train_x = []
    train_y = []
    test_x = []
    test_y = []
    valid_x = []
    valid_y = []
    for root, dirs, names in os.walk(dataset_dir):
        for name in names:
            if '.wav' in name:
                wav_path = os.path.join(root, name)
                # print(SR, wav_path)

                wav_data, sample_rate = process_wav_wavdata(wav_path)
                if wav_data == 'None':
                    continue

                sub_set = wav_path.split(os.sep)[2]
                singer_label = wav_path.split(os.sep)[3]
                if singer_label not in all_singers:
                    all_singers.append(singer_label)
                for item_data in range(0, len(wav_data),
                                       int(DATA_HOP * wav_dur * sample_rate)):  # hop size 0.2* wav_dur
                    seg_wav_data = wav_data[item_data:int(item_data + wav_dur * sample_rate)]
                    if len(seg_wav_data) < wav_dur * sample_rate:
                        continue

                    if sub_set == 'train':
                        train_x.append(seg_wav_data)
                        train_y.append(singer_label)
                    elif sub_set == 'valid':
                        valid_x.append(seg_wav_data)
                        valid_y.append(singer_label)
                    elif sub_set == 'test':
                        test_x.append(seg_wav_data)
                        test_y.append(singer_label)
                    else:
                        raise Exception('not train valid test')
    # one hot encode
    label_encoder = preprocessing.LabelEncoder()
    label_encoder.fit(all_singers)
    joblib.dump(label_encoder, label_encoder_path)
    train_y = to_categorical(label_encoder.transform(train_y))
    test_y = to_categorical(label_encoder.transform(test_y))
    valid_y = to_categorical(label_encoder.transform(valid_y))
    class_num = len(os.listdir(os.path.join(dataset_dir, 'train')))
    return array(train_x), train_y, array(test_x), test_y, array(valid_x), valid_y, class_num


def class_to_real_label(y_predict_class):
    # 从预测的类别整数:0,1,2 解析原始类别:如歌手1,歌手2,歌手3
    label_encoder = joblib.load(label_encoder_path)
    y_predict_label = label_encoder.inverse_transform(y_predict_class)

    return y_predict_label


def roc_auc_score_multiclass(actual_class, pred_class, average="macro"):
    # creating a set of all the unique classes using the actual class list
    unique_class = set(actual_class)
    roc_auc_dict = {}
    for per_class in unique_class:
        # creating a list of all the classes except the current class
        other_class = [x for x in unique_class if x != per_class]

        # marking the current class as 1 and all other classes as 0
        new_actual_class = [0 if x in other_class else 1 for x in actual_class]
        new_pred_class = [0 if x in other_class else 1 for x in pred_class]

        # using the sklearn metrics method to calculate the roc_auc_score
        roc_auc = roc_auc_score(new_actual_class, new_pred_class, average=average)
        roc_auc_dict[per_class] = roc_auc

    return roc_auc_dict


def sn_plot_confusion_matrix(y_true, y_pred, level):
    label_encoder=joblib.load(label_encoder_path)
    ytick_lable=label_encoder.inverse_transform(range(len(label_encoder.classes_)))
    xtick_label=[item[:2] for item in ytick_lable]
    plt.figure()
    sn.set()
    cm = confusion_matrix(y_true, y_pred)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    sn.heatmap(cm,xticklabels=xtick_label,yticklabels=ytick_lable)# TODO
    plt.savefig("tmp/sn_cm_%s.svg" % level, format='svg')
    return 0


def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues, level='segment'):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    # classes = classes[unique_labels(y_true)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    # print(cm)
    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    np.set_printoptions(precision=2)
    fig.savefig('tmp/confusion_matric_%s_%s.svg' % (level, dataset_name), dpi=400, format='svg')
    return ax


def get_err_right(test_y_class, predict_y_class):
    right, err = 0, 0
    for test, predict in zip(test_y_class, predict_y_class):
        if test == predict:
            right += 1
        else:
            err += 1
    print('total:%d\nright:%d\t%.3f\nerr"%d\t%.3f\n' % (
        right + err, right, right / (right + err), err, err / (right + err)))
    return right, err


def evaluate_report(test_y_class, predict_y_class, level='segment'):
    right, err = get_err_right(test_y_class, predict_y_class)
    print('%s\tlevel results:' % level)
    print("ground  truth:", test_y_class)
    print("predict lable:", predict_y_class)
    class_names = class_to_real_label(range(len(set(test_y_class))))
    # accuracy: (tp + tn) / (p + n)
    accuracy = accuracy_score(test_y_class, predict_y_class)
    print('Accuracy: %f' % accuracy)
    # precision tp / (tp + fp)
    precision = precision_score(test_y_class, predict_y_class, average="macro")
    print('Precision: %f' % precision)
    # recall: tp / (tp + fn)
    recall = recall_score(test_y_class, predict_y_class, average="macro")
    print('Recall: %f' % recall)
    # f1: 2 tp / (2 tp + fp + fn)
    f1 = f1_score(test_y_class, predict_y_class, average="macro")
    print('F1 score: %f' % f1)

    # kappa
    kappa = cohen_kappa_score(test_y_class, predict_y_class)
    # print('Cohens kappa: %f' % kappa)
    # ROC AUC
    auc = roc_auc_score_multiclass(test_y_class, predict_y_class)  # predict_y_prob)
    # print('ROC AUC: ', auc)
    # confusion matrix
    matrix = confusion_matrix(test_y_class, predict_y_class)
    # print(matrix)
    joblib.dump((test_y_class, predict_y_class, class_names), 'tmp/cm_%s_%s.joblib' % (level, dataset_name))
    plot_confusion_matrix(test_y_class, predict_y_class, classes=class_names, normalize=True,
                          title='Normalized confusion matrix', level=level)
    sn_plot_confusion_matrix(test_y_class, predict_y_class, level)

    return accuracy, recall, precision, f1, kappa, auc, matrix, right, err


def test_each_song_with_one_lable(wnc):
    ground_truth = []
    predict_singer = []
    for root, dirs, names in os.walk(os.path.join(dataset_dir, 'test')):
        for name in names:
            if '.wav' in name:
                wav_path = os.path.join(root, name)
                print(SR, wav_path)
                wav_data, sample_rate = process_wav_wavdata(wav_path)
                if wav_data == 'None':
                    continue

                singer_label = wav_path.split(os.sep)[3]
                ground_truth.append(singer_label)
                data_x = []
                for item_data in range(0, len(wav_data), int(wav_dur * sample_rate)):
                    seg_wav_data = wav_data[item_data:int(item_data + wav_dur * sample_rate)]
                    if len(seg_wav_data) < wav_dur * sample_rate:
                        continue
                    data_x.append(seg_wav_data)
                if NORMALIZE:
                    data_x = std_scale.transform(array(data_x))
                else:
                    data_x = array(data_x)
                wav_results_prob = wnc.predict(data_x)

                wav_prob = np.sum(wav_results_prob, axis=0)
                wav_predict_num = wav_prob.argmax()

                predict_singer.append(wav_predict_num)

    predict_singer_lable = class_to_real_label(predict_singer)
    accuracy, recall, precision, f1, kappa, auc, matrix, right, err = evaluate_report(ground_truth,
                                                                                      predict_singer_lable,
                                                                                      level='song')

    return accuracy, recall, precision, f1, kappa, auc, matrix, right, err


if __name__ == '__main__':
    if len(sys.argv) > 1:
        wav_dur = float(sys.argv[1])
    res_csv = open('results/differ_segments.csv', 'a')
    writer = csv.writer(res_csv)

    res_str = '%.2f' % wav_dur
    start = time.time()
    dataset_name = dataset_dir.split(os.sep)[-1]
    # print('dataset...')
    train_x, train_y, test_x, test_y, valid_x, valid_y, class_num = get_wav_dataset(dataset_dir)
    if SHUFFLE:
        train_x, train_y = shuffle(train_x, train_y, random_state=0)  # shuffle Training Data
    if NORMALIZE:
        std_scale = preprocessing.StandardScaler().fit(train_x)  # Normalize Training Data
        train_x = std_scale.transform(train_x)
        test_x = std_scale.transform(test_x)
        valid_x = std_scale.transform(valid_x)

    wnc = WaveNetClassifier((int(wav_dur * SR),), (class_num,), kernel_size=2, dilation_depth=9, n_filters=40,
                            task='classification', load=load_true, load_dir=model_dir)
    # print('train...')
    if retrain_true:
        train_history = wnc.fit(train_x, train_y, validation_data=(valid_x, valid_y), epochs=Train_Epochs,
                                batch_size=32, optimizer='adam',
                                save=True,
                                save_dir=model_dir)
        # summarize history for accuracy
        fig_acc = plt.figure()
        plt.plot(train_history.history['acc'])
        plt.plot(train_history.history['val_acc'])
        plt.title('model accuracy')
        plt.ylabel('accuracy')
        plt.xlabel('epoch')
        plt.legend(['train', 'valid'], loc='upper left')
        fig_acc.savefig('tmp/history_acc_%s.svg' % dataset_name, format='svg')
        # summarize history for loss
        fig_loss = plt.figure()
        plt.plot(train_history.history['loss'])
        plt.plot(train_history.history['val_loss'])
        plt.title('model loss')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'valid'], loc='upper left')
        fig_loss.savefig('tmp/history_loss_%s.svg' % dataset_name, format='svg')
    # print('predict...')
    y_pred_prob = wnc.predict(test_x)
    predict_y_class = y_pred_prob.argmax(axis=-1)
    test_y_class = test_y.argmax(axis=-1)
    accuracy, recall, precision, f1, kappa, auc, matrix, right, err = evaluate_report(test_y_class, predict_y_class,
                                                                                      level='segment')
    res_str += ',%.3f,%.3f,%.3f,%.3f,%d,%d' % (accuracy, recall, precision, f1, right, err)
    collect_result_dict = {}
    collect_result_dict['segment_level'] = {'accuracy': accuracy,
                                            'recall': recall,
                                            'precision': precision,
                                            'f1': f1,
                                            'kappa': kappa,
                                            'auc': auc,
                                            'matrix': matrix,
                                            'fig_acc': 'tmp/history_acc_%s.svg' % dataset_name,
                                            'fig_loss': 'tmp/history_loss_%s.svg' % dataset_name,
                                            'fig_cm': 'tmp/confusion_matric_segment_%s.svg' % dataset_name,
                                            'plot_cm_need': 'tmp/cm_segment_%s.joblib' % dataset_name,
                                            }

    accuracy, recall, precision, f1, kappa, auc, matrix, right, err = test_each_song_with_one_lable(wnc)
    res_str += ',%.3f,%.3f,%.3f,%.3f,%d,%d' % (accuracy, recall, precision, f1, right, err)
    collect_result_dict['song_level'] = {'accuracy': accuracy,
                                         'recall': recall,
                                         'precision': precision,
                                         'f1': f1,
                                         'kappa': kappa,
                                         'auc': auc,
                                         'matrix': matrix,
                                         'fig_acc': 'tmp/history_acc_%s.svg' % dataset_name,
                                         'fig_loss': 'tmp/history_loss_%s.svg' % dataset_name,
                                         'fig_cm': 'tmp/confusion_matric_song_%s.svg' % dataset_name,
                                         'plot_cm_need': 'tmp/cm_song_%s.joblib' % dataset_name,
                                         }

    joblib.dump(collect_result_dict, 'tmp/collect_result_dict_%s.joblib' % dataset_name)
    end = time.time()
    print('it takes %.2fs' % (end - start))
    res_str += ',%.2f' % (end - start)
    writer.writerow(res_str.split(','))
