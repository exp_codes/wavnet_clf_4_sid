# coding=utf-8
import time

import keras.backend.tensorflow_backend as KTF
import tensorflow as tf
from keras.models import load_model

Sample_Rate = 16000
verbose_true = True
K_session_clear = True
GPU_ID = 1


# if you want to train a new model, please set  load_model_path = ""
class print_colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


print(print_colors.WARNING + 'USING GPU_ID:%d ' % GPU_ID + print_colors.ENDC)
session = tf.Session(graph=tf.Graph())
KTF.set_session(tf.compat.v1.Session(config=tf.compat.v1.ConfigProto(device_count={'gpu': GPU_ID})))
import librosa

import numpy as np


def get_mfcc(wav_path, sample_rate=0):
    if type(wav_path) == str:
        y, sr = librosa.load(wav_path, sr=Sample_Rate)
    else:
        y = wav_path
        sr = sample_rate
    mfcc = librosa.feature.mfcc(y=y, sr=sr, n_mfcc=20, n_fft=int(sr * 0.04), hop_length=int(sr * 0.02))
    return mfcc, sr


def svd_filter_vocal_segments(vocal_data, sr):
    start = time.time()
    feat_data, _ = get_mfcc(vocal_data, sr)
    feat_data = feat_data.T
    feat_data = feat_data / abs(feat_data).max(axis=0)
    time_step = 29
    finalX = []
    end = time.time()
    if verbose_true:
        print(print_colors.FAIL + "svd  prepare input takes %.2f" % (end - start) + print_colors.ENDC)
    start = time.time()
    for item_y in range(0, np.shape(feat_data)[0], time_step):
        if len(feat_data[item_y:item_y + time_step]) == time_step:
            finalX.append(feat_data[item_y:item_y + time_step])
    finalX = np.array(finalX).reshape((-1, time_step, 20))
    end = time.time()
    if verbose_true:
        print(print_colors.FAIL + "svd  reshape input takes %.2f" % (end - start) + print_colors.ENDC)
    start = time.time()

    svd_model_saved_path = 'svd_lstm.model'
    svd_model_weights_saved_path = 'svd_lstm.weights'
    svd_model = load_model(svd_model_saved_path)
    svd_model.load_weights(svd_model_weights_saved_path)
    svd_model.compile(loss='binary_crossentropy',
                      optimizer='adam',
                      metrics=['binary_accuracy'])
    end = time.time()
    if verbose_true:
        print(print_colors.FAIL + "svd  load model takes %.2f" % (end - start) + print_colors.ENDC)
    start = time.time()
    predictY = svd_model.predict_classes(finalX)
    if K_session_clear:
        KTF.clear_session()  # 避免循环加载模型时间递增
    end = time.time()
    if verbose_true:
        print(print_colors.FAIL + "svd  predict takes %.2f" % (end - start) + print_colors.ENDC)
    # print(predictY)
    new_vocal_data = []
    total_seg = len(predictY)
    total_samples = len(vocal_data)
    for y_item in range(total_seg):
        if predictY[y_item] == 1:
            new_vocal_data.append(
                vocal_data[int((y_item / total_seg) * total_samples):int(((y_item + 1) / total_seg) * total_samples)])

    return np.concatenate(new_vocal_data), sr
